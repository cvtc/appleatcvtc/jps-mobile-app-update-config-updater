JPS Carousel Version Smart Group Updater
=======================================
Updates the following two fields in all unexcluded mobile apps to be true:
* Schedule Jamf Pro to automatically check the App Store for app updates
* Automatically Force App Updates

## Reasoning for script
On occasion, an update to Jamf Pro will contain a defect which causes desired configuration for mobile device apps to not remain consistent with how it was initially set. Most notably this has been an issue with settings involving app updates and update checking. Usually this seems to result in either apps not checking for available App Store updates or the Automatically Force App Updates box to get unchecked.

## Usage

### GitLab
Update the EXCLUDED_IDS variable in jps_mobile_app_update_config_updater to include
any mobile apps that you want excluded from these settings

Uses base Azure Function setup, consult [general documentation.](https://gitlab.com/cvtc/appleatcvtc/project-templates/-/tree/main/Python%20Azure%20Functions?ref_type=heads)

### Local
Export the variables listed below into your environment:
* All variable values are described below.
* The first command will make your bash history ignore commands with whitespace in front of them. This will make it so your JAMF_PASSWORD isn't in your terminal history.
```console
setopt HIST_IGNORE_SPACE
export JPS_URL=https://example.jamf.com
export JPS_USERNAME=exampleUsername
 export JPS_PASSWORD=examplePassword
```

Edit the EXCLUDED_IDS variable in jps_mobile_app_update_config_updater.py to include the mobile apps that you want excluded from this script.
Example:
```python
EXCLUDED_IDS = [1001, 1002, 1003]
```

Run these commands to install all dependencies and run the script:
```console
pip install pipenv
pipenv install --deploy
pipenv run python ./jps_mobile_app_update_config_updater.py
```

## Variables

### JPS_URL
* URL of your JPS Server
> https://example.jamfcloud.com

### JPS_USERNAME / JPS_PASSWORD
JPS account credentials with the following permissions set:
* Mobile Device Apps
  * Read
  * Update

### EXCLUDED_IDS
Mobile app IDs that you want to exclude from the script
> EXCLUDED_IDS = [1001, 1002, 1003]

## Authors
Bryan Weber (bweber26@cvtc.edu)

## Copyright
JPS Mobile App Update Config Updater is Copyright 2023 Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.