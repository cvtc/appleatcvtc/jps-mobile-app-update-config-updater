import azure.functions as func

import jps_mobile_app_update_config_updater

app = func.FunctionApp()


@app.schedule(
    schedule="0 0 6 * * *", arg_name="myTimer", run_on_startup=False, use_monitor=False
)
def TimerTrigger(myTimer: func.TimerRequest) -> None:
    jps_mobile_app_update_config_updater.main()
