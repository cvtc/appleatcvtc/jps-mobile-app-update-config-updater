import logging
from os import environ

from jps_api_wrapper.classic import Classic
from xmltodict import parse, unparse

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def main():
    JPS_URL = environ["JPS_URL"]
    JPS_USERNAME = environ["JPS_USERNAME"]
    JPS_PASSWORD = environ["JPS_PASSWORD"]
    EXCLUDED_IDS = [
        54,
        86,
        89,
        104,
        206,
        228,
        386,
        428,
        473,
        480,
        511,
        515,
        516,
        538,
        541,
        542,
    ]

    with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic:
        mobile_apps = classic.get_mobile_device_applications()
        for mobile_app in mobile_apps["mobile_device_applications"]:
            app_updated = False
            if mobile_app["id"] not in EXCLUDED_IDS:
                mobile_app_details = parse(
                    classic.get_mobile_device_application(
                        mobile_app["id"], data_type="xml"
                    )
                )
                unparse(mobile_app_details)
                app = mobile_app_details["mobile_device_application"]["general"]

                # Automatically Force App Updates
                if app["keep_app_updated_on_devices"] != "true":
                    app["keep_app_updated_on_devices"] = "true"
                    app_updated = True

                # Schedule Jamf Pro to automatically check the App Store for
                # app updates
                if app["keep_description_and_icon_up_to_date"] != "true":
                    app["keep_description_and_icon_up_to_date"] = "true"
                    app_updated = True

                mobile_app_details = unparse(mobile_app_details)

                # Update the app if anything needs to be changed
                if app_updated:
                    classic.update_mobile_device_application(
                        mobile_app_details.encode(), mobile_app["id"]
                    )
                    logging.info(
                        f"Updated app {mobile_app['id']}: {mobile_app['name']}"
                    )


if __name__ == "__main__":
    main()
